#zad1
def evens(list):
    sum = 0
    for i in list:
        if i % 2 == 0:
            sum += 1
    return sum

#zad2
def centeredAverage(list):
    min = list[0]
    max = list[0]
    for i in list:
        if i > max: max = i
        if i < min: min = i

    suma = 0
    flag = 0
    for i in list:
        suma += i
    suma = suma - min - max
    return suma / (len(list) - 2)

#zad3
def check2s(list):
    for i in range(len(list) - 1):
        if list[i] == 2 and list[i + 1] == 2:
            return ('True')
    return ('False')
        
#zad4
def upperLowerDigits(string):
    upper = sum(1 for c in string if c.isupper())
    lower = sum(1 for c in string if c.islower())
    numOfNumbers = sum(1 for c in string if c.isnumeric())
    return (upper, lower, numOfNumbers)



def stringListToInt():
    string = str(input('Unesite brojeve (odvojene zarezom)'))
    lista = string.split(',')
    lista = [int(i) for i in lista]
    return lista

def main():
    lista = stringListToInt()

    print('number of even numbers: ', evens(lista))
    print('centered average: ', centeredAverage(lista))
    print('is there a 2 next to a 2: ', check2s(lista))
    print('upper case, lower case, digits: ', upperLowerDigits("asdf98CXX21grrr"))

if __name__ == "__main__":
    main()
