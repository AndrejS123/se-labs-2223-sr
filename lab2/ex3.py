from ast import dump
import csv
import json

employeeDict = []

with open('ex2-text.csv') as file:
    reader = csv.DictReader(file)
    for row in reader:
        employeeDict.append(row)

with open('ex4-txt.js', "w", encoding="utf_8") as f:
    json.dump(employeeDict, f)